export function Shuffle(array: any) {
  var currentIndex = array.length,
    temporaryValue,
    randomIndex;

  while (0 !== currentIndex) {
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }

  return array;
}

export function TimetoString(min: number | undefined, sec: number | undefined) {
  let minit: string = "";
  let sekon: string = "";
  if (min !== undefined) {
    min >= 10 ? (minit = String(min)) : (minit = `0${String(min)}`);
  } else {
    minit = `00`;
  }
  if (sec !== undefined) {
    sec >= 10 ? (sekon = String(sec)) : (sekon = `0${String(sec)}`);
  } else {
    sekon = `00`;
  }
  return `${minit} : ${sekon}`;
}
