import { Dispatch, SetStateAction } from "react";

export interface SoalInterface {
  order: number;
  question: string;
  dijawab: boolean;
  jawaban: string;
  answer: number;
  options: { text: string; order: number }[];
}

export interface TestInfo {
  nama: string;
  id: number | string;
  index: number;
  subtes: string;
  duration: number;
  image: string;
  timeStamp: number;
  listSubtes: string[];
}

export interface WaktuInterface {
  min: number;
  sec: number;
}

export interface TestInterface {
  refresh: () => void;
  kumpulanSoal: Array<SoalInterface>;
  setSoal: Dispatch<SetStateAction<SoalInterface[]>>;
  testState: TestInfo;
  setTestState: Dispatch<SetStateAction<TestInfo>>;
  waktu: {
    pu: WaktuInterface;
    ppu: WaktuInterface;
    pk: WaktuInterface;
    pmm: WaktuInterface;
    eng: WaktuInterface;
    ma: WaktuInterface;
    fi: WaktuInterface;
    ki: WaktuInterface;
    bi: WaktuInterface;
    sos: WaktuInterface;
    sej: WaktuInterface;
    geo: WaktuInterface;
    eko: WaktuInterface;
  };
  setTimer: Dispatch<SetStateAction<boolean>>;
  nextSubtes: () => void;
  handleRefreshOpen: () => void;
}
