import React, { Dispatch, SetStateAction } from "react";

export interface SoalInterface {
  soal: string;
  no: number;
  dijawab: boolean;
  jawaban: string;
}
export interface TrialInfoProps {
  refresh: () => void;
  kumpulanSoal: Array<SoalInterface>;
  setSoal: Dispatch<
    SetStateAction<
      { no: number; soal: string; dijawab: boolean; jawaban: string }[]
    >
  >;
  index: number;
  setIndex: React.Dispatch<React.SetStateAction<number>>;
}
