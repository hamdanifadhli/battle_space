import "./App.css";
import { useState, useEffect } from "react";
import Login from "./pages/Login";
import Lobby from "./pages/Lobby";
import Over from "./pages/Over";
import Trial from "./pages/Trial";
import Test from "./pages/Test";
import { useCookies, Cookies, CookiesProvider } from "react-cookie";
import { Loading } from "./components/exam_component/Loading";

function App() {
  const [route, setroute] = useState(18);
  const [triger, settriger] = useState(false);
  const [cookies, setCookies] = useCookies(["zhongguan"]);

  // const cobabut = () => {
  //   const d = new Date();
  //   const milis = 10 * 1000; //10detik
  //   const x = new Date(d.getTime() + milis);
  //   setCookies("coba", "syoki bau", { expires: x, sameSite: "lax" });
  //   setCookies("banget", 10, { expires: x, sameSite: "lax" });
  // };

  // const bangetbut = () => {
  //   console.log(cookies.coba, cookies.banget);
  //   console.log({ cookies });
  // };

  useEffect(() => {
    let route = localStorage.getItem("route");
    if (route) {
      setroute(Number(route));
    } else {
      setroute(0);
    }
  }, [triger]);

  useEffect(() => {
    // if (!cookies.zhongguan) {
    //   localStorage.clear();
    //   refresh();
    // }
    let route = localStorage.getItem("route");
    if (typeof route === "string") {
      setroute(Number(route));
    } else {
      setroute(0);
    }
  }, []);

  function refresh() {
    settriger(!triger);
  }

  return (
    <CookiesProvider>
      <div className="App">
        {route === 0 ? (
          <div className="login">
            <Login refresh={refresh} setCookies={setCookies} />
          </div>
        ) : null}
        {route === 1 ? (
          <div className="lobby">
            <Lobby refresh={refresh} />
          </div>
        ) : null}
        {route === 2 ? (
          <div className="trial">
            <Trial refresh={refresh} />
          </div>
        ) : null}
        {route === 3 ? (
          <div className="test">
            <Test refresh={refresh} />
          </div>
        ) : null}
        {route === 4 ? (
          <div className="over">
            <Over />
          </div>
        ) : null}
        {route === 5 ? (
          <div>
            <Loading />
          </div>
        ) : null}
      </div>
    </CookiesProvider>
  );
}

export default App;
