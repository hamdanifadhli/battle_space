import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import {
  Button,
  CardActions,
  Hidden,
  Table,
  TableBody,
  TableRow,
  TableCell,
} from "@material-ui/core";
import { TrialInfoProps } from "../interface/trial";

const useStyles = makeStyles((theme) => ({
  root: {
    textAlign: "center",
    minWidth: "24vw",
    [theme.breakpoints.down("sm")]: {
      minWidth: "90vw",
    },
    margin: "0 auto",
  },
  bullet: {
    display: "inline-block",
    margin: "0 2px",
    transform: "scale(0.8)",
  },
  title: {
    fontSize: 14,
  },
  pos: {
    fontSize: 20,
    marginBottom: 10,
  },
  media: {
    height: 0,
    paddingTop: "40vh",
  },
  image: {
    width: "100%",
    height: "auto",
    [theme.breakpoints.down("sm")]: {
      width: "80%",
    },
    [theme.breakpoints.down("xs")]: {
      width: "100%",
    },
  },
  buttonSelected: {
    backgroundColor: "#3f51b5",
    color: "#FFF",
    "&:hover": {
      backgroundColor: "#fff",
      color: "#3f51b5",
    },
  },
}));

export default function TrialInfo(props: TrialInfoProps) {
  const { refresh, kumpulanSoal, setSoal, index, setIndex } = props;
  const classes = useStyles();
  const name = localStorage.getItem("name");

  function update(index: number) {
    setIndex(index);
  }

  return (
    <Card className={classes.root}>
      <CardContent>
        <Hidden mdUp>
          {kumpulanSoal.map((param, i) => (
            <Button
              className={
                param.dijawab || index === i ? classes.buttonSelected : ""
              }
              variant="outlined"
              color="primary"
              style={{
                margin: "0.2rem",
              }}
              onClick={() => {
                update(i);
              }}
            >
              {i + 1}
            </Button>
          ))}
        </Hidden>
        {/* <CardMedia
          className={classes.media}
          image="https://studybuddy.id/_next/image?url=https%3A%2F%2Fstudybuddy.id%2Fapi%2Fv2%2Ffile%2Fbr5.jpg&w=320&q=75"
          title="Paella dish"
        /> */}
        <div>
          <img src="/undraw_exams_g4ow.svg" alt="" className={classes.image} />
        </div>
        <Typography
          variant="h5"
          component="h1"
          style={{ margin: "0.5rem 0 0 0" }}
        >
          Trial Battle
        </Typography>
        <Table>
          <TableBody>
            <TableRow>
              <TableCell>
                <b>Nama</b>
              </TableCell>
              <TableCell size="small">:</TableCell>
              <TableCell style={{ textTransform: "capitalize" }}>
                {name}
              </TableCell>
            </TableRow>
          </TableBody>
        </Table>
        <Hidden smDown>
          {kumpulanSoal.map((param, i) => (
            <Button
              className={
                param.dijawab || index === i ? classes.buttonSelected : ""
              }
              variant="outlined"
              color="primary"
              style={{ margin: "0.2rem" }}
              onClick={() => {
                update(i);
              }}
            >
              {i + 1}
            </Button>
          ))}
        </Hidden>
      </CardContent>
      <CardActions>
        <Button
          variant="outlined"
          color="primary"
          size="small"
          fullWidth
          style={{ backgroundColor: "#ebf3ff" }}
          onClick={() => {
            localStorage.setItem("route", "1");
            refresh();
          }}
        >
          Kembali
        </Button>
      </CardActions>
    </Card>
  );
}
