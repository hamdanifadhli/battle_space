import { makeStyles, Theme, createStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardContent from "@material-ui/core/CardContent";
import { Button } from "@material-ui/core";
import { SoalInterface, TrialInfoProps } from "../interface/trial";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      minWidth: "72vw",
      margin: "0 auto",
    },
    buttonSelected: {
      backgroundColor: "#3f51b5",
      color: "#FFF",
      "&:hover": {
        backgroundColor: "#fff",
        color: "#3f51b5",
      },
    },
  })
);

export default function TrialQuestion(props: TrialInfoProps) {
  const { refresh, kumpulanSoal, setSoal, index, setIndex } = props;

  const classes = useStyles();

  const jawaban = ["A", "B", "C", "D", "E"];

  const createMarkup = (wow: string) => {
    return { __html: wow };
  };
  function update(index: number, jawab: string) {
    let arr: Array<SoalInterface> = [];
    kumpulanSoal.map((param, i) => {
      if (i === index) {
        arr[i] = {
          ...kumpulanSoal[i],
          dijawab: jawab === kumpulanSoal[i].jawaban ? false : true,
          jawaban: jawab === kumpulanSoal[i].jawaban ? "" : jawab,
        };
      } else {
        arr[i] = kumpulanSoal[i];
      }
    });
    setSoal(arr);
  }

  return (
    <Card className={classes.root}>
      <CardHeader
        title={`Soal ${index + 1}`}
        style={{ backgroundColor: "#e6eaff" }}
      />
      <CardContent>
        <div
          dangerouslySetInnerHTML={createMarkup(kumpulanSoal[index].soal)}
        ></div>
      </CardContent>
      <div style={{ margin: "0 1rem 1rem 1rem" }}>
        {jawaban.map((param) => (
          <Button
            variant="outlined"
            className={
              param === kumpulanSoal[index].jawaban
                ? classes.buttonSelected
                : ""
            }
            color="primary"
            style={{ margin: "0.2rem" }}
            onClick={() => update(index, param)}
          >
            {param}
          </Button>
        ))}
      </div>
    </Card>
  );
}
