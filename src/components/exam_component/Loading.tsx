import {
  Card,
  CardContent,
  Container,
  Fade,
  Grid,
  Typography,
  LinearProgress,
  CardActions,
} from "@material-ui/core";
import { withStyles, Theme, createStyles } from "@material-ui/core/styles";
import { useEffect, useState } from "react";
import Loader from "react-loader-spinner";
import { Shuffle } from "../../functions/exam_func/Loading_help";
import { Quotes } from "./quotes";

export const Loading = () => {
  const [Quotesrand, setQuotesrand] = useState([
    {
      quote:
        "Opportunity does not knock, it presents itself when you beat down the door.",
      author: "- Kyle Chandler",
    },
  ]);
  const [IndexQ, setIndexQ] = useState(0);
  const [progress, setProgress] = useState<number>(100);
  const [Checked, setChecked] = useState(true);
  let Counter: number = 0;
  let Check: boolean = true;
  let meng = IndexQ;
  const run = () => {
    if (Counter < 1) {
      Check = true;
      Counter++;
    }
    if (1 <= Counter && Counter < 25) {
      Counter++;
    }
    if (Counter === 24) {
      Check = false;
    }
    if (Counter === 25) {
      Counter = 0;
      if (meng <= Quotesrand.length - 1) {
        meng = meng + 1;
      } else {
        meng = 0;
      }
    }

    setChecked(Check);
    setIndexQ(meng);
  };
  useEffect(() => {
    setInterval(run, 250);
    setQuotesrand(Shuffle(Quotes));
    setProgress(100);
    const timer = setInterval(() => {
      setProgress((prevProgress) =>
        prevProgress <= 0 ? 100 : prevProgress - 1
      );
    }, 300);
    return () => {
      clearInterval(timer);
    };
  }, []);

  return (
    <div>
      <Container maxWidth="sm" style={{ textAlign: "left" }}>
        <Card
          variant="outlined"
          style={{ marginTop: "30vh", marginBottom: "30vh", padding: "auto 0" }}
        >
          <CardContent style={{ paddingBottom: "24px" }}>
            <Grid container spacing={5}>
              <Grid item xs={12} md={3} style={{ padding: "0" }}>
                <Loader
                  type="Rings"
                  color="#00BFFF"
                  height={115}
                  width={115}
                  style={{
                    flex: "1",
                    alignItems: "center",
                    justifyContent: "center",
                    textAlign: "center",
                    marginTop: "1rem",
                  }}
                />
              </Grid>
              <Grid
                item
                xs={12}
                md={9}
                style={{
                  marginTop: "auto",
                  marginBottom: "auto",
                  textAlign: "center",
                  padding: "0",
                }}
              >
                <div style={{ margin: "1rem" }}>
                  <Fade in={Checked}>
                    <Typography variant="h6">
                      {Quotesrand[IndexQ].quote}
                    </Typography>
                  </Fade>
                  <Fade in={Checked}>
                    <Typography variant="subtitle1">
                      {Quotesrand[IndexQ].author}
                    </Typography>
                  </Fade>
                </div>
              </Grid>
            </Grid>
          </CardContent>
          <CardActions>
            <BorderLinearProgress
              style={{ width: "100%", height: 5 }}
              variant={"determinate"}
              value={progress}
            ></BorderLinearProgress>
          </CardActions>
        </Card>
      </Container>
    </div>
  );
};

const BorderLinearProgress = withStyles((theme: Theme) =>
  createStyles({
    root: {
      height: 10,
      borderRadius: 5,
    },
    colorPrimary: {
      backgroundColor:
        theme.palette.grey[theme.palette.type === "light" ? 200 : 700],
    },
    bar: {
      borderRadius: 5,
      backgroundColor: "#1a90ff",
    },
  })
)(LinearProgress);
