export const Quotes = [
  {
    quote:
      "Opportunity does not knock, it presents itself when you beat down the door.",
    author: "- Kyle Chandler",
  },
  {
    quote:
      "Never give up on something that you can’t go a day without thinking about.",
    author: "- Winston Churchill",
  },
  {
    quote: "Tuhanmu dan keluargamu adalah semangatmu",
    author: "- Arul Arurul",
  },
  {
    quote: "What we know is not much. What we don't know is enormous",
    author: "- Pierre-Simon Laplace",
  },
  {
    quote: "Bawa santai saja",
    author: "- Asep Balon",
  },
  {
    quote:
      "Kamu tidak bisa kembali dan mengubah awal saat kamu memulainya, tapi kamu bisa memulainya lagi dari di mana kamu berada sekarang dan ubah akhirnya.",
    author: "- C.S Lewis",
  },
  {
    quote:
      "Tanpa cinta, kecerdasan itu berbahaya, dan tanpa kecerdasan, cinta itu tidak cukup",
    author: "- B.J. Habibie",
  },
  {
    quote: "Jangan menjadi sedih, Jadilah lebih baik.",
    author: "- Om Jenggot",
  },
  {
    quote: "Terkadang keinginanmu tak sebaik yang kamu kira.",
    author: "- Roy Hamid",
  },
  {
    quote:
      "Jangan gantungkan cita-citamu kepada orang lain. Berusahalah sampai kamu mendapatkannya",
    author: "- Arul Arurul",
  },
  {
    quote:
      "Mungkin kamu tuh kayak glowstick, harus patah dulu sebelum bisa bersinar",
    author: "- Jadah Sellner",
  },
  {
    quote: "Bisa Bisa Bisa.",
    author: "- Hamdabi",
  },
  {
    quote: "Jangan lupa sembahyang.",
    author: "- Gugus Sulistyo",
  },
  {
    quote:
      "Kerja keras tidak ada gunanya untuk mereka yang tidak percaya pada dirinya sendiri",
    author: "- Naruto",
  },
  {
    quote:
      "If you win you live. If you lose you die. If you don’t fight, you can’t win.",
    author: "- Eren Jaeger",
  },
  {
    quote:
      "The lesson you need to learn right now can’t be taught with words, only with action.",
    author: "- Levi Ackerman",
  },
];
