import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import {
  Button,
  CardActions,
  Dialog,
  DialogContent,
  DialogContentText,
  DialogActions,
  Hidden,
  Table,
  TableRow,
  TableBody,
  TableCell,
  Paper,
} from "@material-ui/core";
import { TestInterface } from "../interface/test";
import { API } from "../configs/API";
import axios from "axios";
import { useEffect, useState } from "react";
import { Shuffle, TimetoString } from "../functions/exam_func/Loading_help";
import axiosRetry from "axios-retry";

const useStyles = makeStyles((theme) => ({
  root: {
    textAlign: "center",
    minWidth: "24vw",
    [theme.breakpoints.down("sm")]: {
      minWidth: "90vw",
    },
    margin: "0 auto",
  },
  bullet: {
    display: "inline-block",
    margin: "0 2px",
    transform: "scale(0.8)",
  },
  title: {
    fontSize: 14,
  },
  pos: {
    fontSize: 20,
    marginBottom: 10,
  },
  media: {
    height: 0,
    paddingTop: "40vh",
  },
  image: {
    width: "100%",
    height: "auto",
    [theme.breakpoints.down("sm")]: {
      width: "80%",
    },
    [theme.breakpoints.down("xs")]: {
      width: "100%",
    },
  },
  buttonSelected: {
    backgroundColor: "#3f51b5",
    color: "#FFF",
    "&:hover": {
      backgroundColor: "#3f51b5",
      color: "#FFF",
    },
  },
}));

export default function InformationCard(props: TestInterface) {
  axiosRetry(axios, {
    retries: 3,
    retryDelay: (retryCount) => {
      return 3000;
    },
  });
  const classes = useStyles();
  const name = localStorage.getItem("name");
  const [open, setOpen] = useState<boolean>(false);
  const handleOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };
  const {
    refresh,
    kumpulanSoal,
    setSoal,
    testState,
    setTestState,
    waktu,
    setTimer,
    nextSubtes,
    handleRefreshOpen,
  } = props;
  function update(index: number) {
    setTestState({ ...testState, index: index });
  }

  useEffect(() => {}, [waktu]);

  const countDown = (subtes: any) => {
    switch (subtes) {
      case "pu":
        return waktu.pu;

      case "ppu":
        return waktu.ppu;

      case "pk":
        return waktu.pk;

      case "pmm":
        return waktu.pmm;

      case "eng":
        return waktu.eng;

      case "ma":
        return waktu.ma;

      case "fi":
        return waktu.fi;

      case "ki":
        return waktu.ki;

      case "bi":
        return waktu.bi;

      case "sos":
        return waktu.sos;

      case "sej":
        return waktu.sej;

      case "eko":
        return waktu.eko;

      case "geo":
        return waktu.geo;

      default:
        return { min: 0, sec: 0 };
    }
  };

  function NextSubject() {
    let a = testState.listSubtes;
    a.shift();
    let b = a[0];
    setTestState({
      ...testState,
      listSubtes: a !== undefined ? a : testState.listSubtes,
      duration: 999999,
      index: 0,
      subtes: b,
    });
    if (testState.id === 0) {
    } else {
      axios
        .get(
          `https://wahyuboard.my.id/study/content/test/get/subtest/${b}/${testState.id}`
        )
        .then(function (response) {
          setSoal(Shuffle(response.data.question));
          let test = {
            ...testState,
            duration: response.data.duration,
            timeStamp: new Date().getTime(),
            subtes: b,
            index: 0,
          };
          setTimeout(() => {
            setTestState(test);
            setTimer(false);
            refresh();
            setTimeout(() => {
              window.location.reload();
            }, 1500);
          }, 1000);
        })
        .catch(function (error) {
          console.log(error);
          handleRefreshOpen();
        });
    }
  }
  function EndTest() {
    localStorage.setItem("route", "4");
    localStorage.removeItem("lalala");
    localStorage.removeItem("u_id");
    localStorage.removeItem("t_id");
    localStorage.removeItem("battleToken");
    localStorage.removeItem("kecapxyz");
    localStorage.removeItem("test");
    localStorage.removeItem("name");
    refresh();
  }

  return (
    <div>
      <Card className={classes.root}>
        <CardContent>
          <Hidden mdUp>
            {kumpulanSoal.map((param, i) => (
              <Button
                className={
                  param.dijawab || testState.index === i
                    ? classes.buttonSelected
                    : ""
                }
                variant="outlined"
                color="primary"
                style={{ margin: "0.2rem" }}
                onClick={() => {
                  update(i);
                }}
              >
                {i + 1}
              </Button>
            ))}
          </Hidden>
          {/* <CardMedia
          className={classes.media}
          image="https://studybuddy.id/_next/image?url=https%3A%2F%2Fstudybuddy.id%2Fapi%2Fv2%2Ffile%2Fbr5.jpg&w=320&q=75"
          title="Paella dish"
        /> */}
          <Paper
            style={{
              margin: " 0 auto",
              justifyContent: "center",
              alignItems: "center",
              display: "flex",
              background: "rgba(0,0,0,0)",
              flexDirection: "column",
            }}
            elevation={0}
          >
            <img src={`${testState.image}`} className={classes.image} alt="" />
          </Paper>
          <Typography
            variant="h5"
            component="h1"
            style={{ margin: "0.5rem 0 0 0" }}
          >
            {testState.nama}
          </Typography>
          <Typography className={classes.pos} color="textSecondary">
            {testState.listSubtes[0]}
          </Typography>
          <Table>
            <TableBody>
              <TableRow>
                <TableCell>
                  <b>Nama</b>
                </TableCell>
                <TableCell size="small">:</TableCell>
                <TableCell style={{ textTransform: "capitalize" }}>
                  {name}
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell>
                  <b>Sisa Waktu</b>
                </TableCell>
                <TableCell size="small">:</TableCell>
                <TableCell style={{ textTransform: "capitalize" }}>
                  {/* {TimetoString(
                  typeof countDown(testState.subtes)?.min === "number"
                    ? countDown(testState.subtes)?.min
                    : 0,
                  typeof countDown(testState.subtes)?.sec === "number"
                    ? countDown(testState.subtes)?.min
                    : 0
                )} */}
                  {/* {`${
                    countDown(testState.listSubtes[0])?.min || 0 >= 10
                      ? countDown(testState.listSubtes[0])?.min || 0
                      : `0${countDown(testState.listSubtes[0])?.min}`
                  } : ${
                    countDown(testState.listSubtes[0])?.sec || 0 >= 10
                      ? countDown(testState.listSubtes[0])?.sec || 0
                      : `0${countDown(testState.listSubtes[0])?.sec}`
                  }`} */}
                  {TimetoString(
                    countDown(testState.listSubtes[0]).min,
                    countDown(testState.listSubtes[0]).sec
                  )}
                </TableCell>
              </TableRow>
            </TableBody>
          </Table>
          <Hidden smDown>
            {kumpulanSoal.map((param, i) => (
              <Button
                className={
                  param.dijawab || testState.index === i
                    ? classes.buttonSelected
                    : ""
                }
                variant="outlined"
                color="primary"
                style={{ margin: "0.2rem" }}
                onClick={() => {
                  update(i);
                }}
              >
                {i + 1}
              </Button>
            ))}
          </Hidden>
        </CardContent>
        <CardActions>
          <Button
            variant="outlined"
            color="primary"
            size="small"
            fullWidth
            style={{ backgroundColor: "#ebf3ff" }}
            onClick={() => {
              handleOpen();
            }}
          >
            {testState.listSubtes.length > 1 ? "Next Section" : "End Test"}
          </Button>
        </CardActions>
      </Card>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogContent>
          {testState.listSubtes.length > 1 ? (
            <DialogContentText id="alert-dialog-description">
              Apakah anda yakin untuk melanjutkan ke subtes berikutnya? <br />
              Jika anda telah melanjutkan ke subtes berikutnya anda tidak bisa
              lagi kembali ke subtes ini
            </DialogContentText>
          ) : (
            <DialogContentText id="alert-dialog-description">
              Apakah anda yakin untuk menyelesaikan test ini? <br />
            </DialogContentText>
          )}
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Tidak Setuju
          </Button>
          <Button
            onClick={() => {
              testState.listSubtes.length > 1 ? NextSubject() : EndTest();
              handleClose();
            }}
            color="primary"
            autoFocus
          >
            Setuju
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
