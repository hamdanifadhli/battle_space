import { makeStyles, Theme, createStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardContent from "@material-ui/core/CardContent";
import { TestInterface, SoalInterface } from "../interface/test";
import { Button } from "@material-ui/core";
import axios from "axios";
import { API } from "../configs/API";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      minWidth: "72vw",
      margin: "0 auto",
    },
    buttonSelected: {
      backgroundColor: "#3f51b5",
      color: "#FFF",
      "&:hover": {
        backgroundColor: "#3f51b5",
        color: "#FFF",
      },
    },
  })
);

export default function Examquestion(props: TestInterface) {
  const classes = useStyles();
  const { refresh, kumpulanSoal, setSoal, testState, setTestState, waktu } =
    props;
  const createMarkup = (wow: string) => {
    return { __html: wow };
  };

  const mathSVG = (wow: string) => {
    if (wow.lastIndexOf("<math") >= 0) {
      var arrStr: any[] = wow.split(/(<math)(.*?)(<\/math=?>)/);
      var html: string = "";
      for (let i = 0; i < arrStr.length; i++) {
        if (arrStr[i] === "<math") {
          arrStr[
            i
          ] = `<img src='https://www.wiris.net/demo/editor/render.svg?mml=${arrStr[i]}`;
        } else if (arrStr[i] === "</math>") {
          arrStr[i] = `${arrStr[i]}&backgroundColor:%23fff'>`;
        } else if (i === 4) {
          if (arrStr[4].lastIndexOf("<math") >= 0) {
            arrStr[4] = mathSVG(arrStr[4]);
          }
        } else if (i === 2) {
          if (arrStr[2].lastIndexOf("<math") >= 0) {
            arrStr[2] = mathSVG(arrStr[2]);
          }
        }
        if (arrStr[i].lastIndexOf(">&#60;</") >= 0) {
          arrStr[i] = arrStr[i].replaceAll(">&#60;</", ">%26lt%3B</");
          // console.log(arrStr[i]);
        }
        if (arrStr[i].lastIndexOf(">&#60</") >= 0) {
          arrStr[i] = arrStr[i].replaceAll(">&#60</", ">%26lt%3B</");
          // console.log("hoho");
          // console.log(arrStr[i]);
        }
        if (arrStr[i].lastIndexOf("><</") >= 0) {
          arrStr[i] = arrStr[i].replaceAll("><</", ">%26lt%3B</");
        }
        if (arrStr[i].lastIndexOf(">+</") >= 0) {
          console.log("hawoo");
          arrStr[i] = arrStr[i].replaceAll(">+</", ">%2B</");
        }
        if (arrStr[i].lastIndexOf(`>'<`) >= 0) {
          arrStr[i] = arrStr[i].replaceAll(`>'<`, ">&#8216<");
        }
        html = html + arrStr[i];
      }
      return html;
    } else return wow;
  };

  const jawaban = ["A", "B", "C", "D", "E"];

  async function update(index: number, jawab: string) {
    let arr: Array<SoalInterface> = [];
    console.log(index, jawab);
    axios
      .post(
        `https://wahyuboard.my.id/study/score/submit`,
        {
          test_id: localStorage.getItem("t_id"),
          abbv: testState.subtes,
          no_soal: kumpulanSoal[index].order,
          answer: jawaban.indexOf(jawab) === kumpulanSoal[index].answer,
          student_answer: jawab,
        },
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("totken")}`,
          },
        }
      )
      .then(function (response) {
        kumpulanSoal.map((param, i) => {
          if (i === index) {
            arr[i] = {
              ...kumpulanSoal[i],
              dijawab: jawab === kumpulanSoal[i].jawaban ? false : true,
              jawaban: jawab === kumpulanSoal[i].jawaban ? "" : jawab,
            };
          } else {
            arr[i] = kumpulanSoal[i];
          }
        });
        setSoal(arr);
      })
      .catch(function (error) {
        alert("Network error");
      });
  }

  return (
    <Card className={classes.root}>
      <CardHeader
        title={`Nomor ${testState.index + 1}`}
        style={{ backgroundColor: "#e6eaff" }}
      />
      <CardContent>
        <div
          dangerouslySetInnerHTML={createMarkup(
            mathSVG(
              kumpulanSoal[testState.index].question
              // "<math><mo><</mo></math>"
            )
          )}
        ></div>
        <table style={{ display: "block", verticalAlign: "top" }}>
          {kumpulanSoal[testState.index].options.map((val, i) => {
            return (
              <tr
                style={{
                  overflow: "auto",
                  display: "inline-block",
                  width: "100%",
                }}
              >
                <td style={{ whiteSpace: "nowrap" }}>{jawaban[val.order]}. </td>
                <td dangerouslySetInnerHTML={createMarkup(val.text)}></td>
              </tr>
            );
          })}
        </table>
      </CardContent>
      <div style={{ margin: "0 1rem 1rem 1rem" }}>
        {jawaban.map((param) => (
          <Button
            variant="outlined"
            className={
              param === kumpulanSoal[testState.index].jawaban
                ? classes.buttonSelected
                : ""
            }
            color="primary"
            style={{ margin: "0.2rem" }}
            onClick={() => update(testState.index, param)}
          >
            {param}
          </Button>
        ))}
      </div>
    </Card>
  );
}
