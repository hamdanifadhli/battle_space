import {
  Avatar,
  Card,
  CardContent,
  CardHeader,
  Container,
  TextField,
  Button,
  Paper,
  InputAdornment,
} from "@material-ui/core";
import AccountCircleTwoToneIcon from "@material-ui/icons/AccountCircleTwoTone";
import VpnKeyTwoToneIcon from "@material-ui/icons/VpnKeyTwoTone";
import { makeStyles, Theme, createStyles } from "@material-ui/core/styles";
import { useState } from "react";
import axios from "axios";
import { API } from "../configs/API";
import { v4 } from "uuid";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    marginCard: {
      [theme.breakpoints.up("xs")]: {
        margin: "0 1rem",
      },
      [theme.breakpoints.up("sm")]: {
        margin: "0 7.5rem",
      },
    },
  })
);

interface LoginProps {
  refresh: () => void;
  setCookies: (name: string, value: any, options?: any | undefined) => void;
}

export default function Login(props: LoginProps) {
  const { refresh, setCookies } = props;

  const setExpire = () => {
    const d = new Date();
    const milis = 2 * 24 * 60 * 60 * 1000; //10detik
    const x = new Date(d.getTime() + milis);
    let r = v4();
    setCookies("zhongguan", `${r}`, { expires: x, sameSite: "lax" });
  };

  const classes = useStyles();
  const [formState, setformState] = useState({
    username: "",
    password: "",
  });
  const [selectState, setselectState] = useState({
    username: false,
    password: false,
  });
  const [errorState, seterrorState] = useState({
    username: {
      error: false,
      message: "",
    },
    password: {
      error: false,
      message: "",
    },
  });
  const headers = {
    "Access-Control-Allow-Origin": "*",
    "Content-Type": "application/json",
  };

  const handleLogin = async () => {
    if (formState.username === "" || formState.password === "") {
      seterrorState({
        username: {
          error: formState.username === "",
          message:
            formState.username === "" ? "Username tidak boleh kosong" : "",
        },
        password: {
          error: formState.password === "",
          message:
            formState.password === "" ? "Password tidak boleh kosong" : "",
        },
      });
    } else {
      axios
        .post(
          `${API}/student/newtestauth`,
          {
            username: formState.username,
            password: formState.password,
          },
          { headers: headers }
        )
        .then(function (response) {
          localStorage.setItem("route", "1");
          localStorage.setItem("battleToken", response.data.token);
          localStorage.setItem("u_id", response.data.data.user_id);
          localStorage.setItem("t_id", response.data.data.test_id);
          localStorage.setItem("name", response.data.data.name);
          localStorage.setItem("test", JSON.stringify(response.data.data.test));
          setExpire();
          refresh();
        })
        .catch(function (err) {
          seterrorState({
            username: {
              error: true,
              message: "Username atau Password salah",
            },
            password: {
              error: true,
              message: "Username atau Password salah",
            },
          });
        })
        .then(function () {});
    }
  };

  return (
    <div
      style={{
        justifyContent: "center",
        display: "flex",
        background: "rgba(0,0,0,0)",
        flexDirection: "column",
        height: "100vh",
      }}
    >
      <Container maxWidth="sm">
        <Card className={classes.marginCard}>
          <CardHeader
            style={{
              background: "linear-gradient(to right, #5b86e5, #36d1dc)",
              color: "#FFF",
            }}
            title="Login"
          />
          <CardContent>
            <form
              onSubmit={(e) => {
                e.preventDefault();
                handleLogin();
              }}
            >
              <TextField
                fullWidth
                label="Username Battle"
                value={formState.username}
                onChange={(e) => {
                  setformState({ ...formState, username: e.target.value });
                  seterrorState({
                    ...errorState,
                    username: { ...errorState.username, error: false },
                  });
                }}
                variant="outlined"
                style={{ marginBottom: "2rem" }}
                onFocus={() =>
                  setselectState({ ...selectState, username: true })
                }
                onBlur={() =>
                  setselectState({ ...selectState, username: false })
                }
                error={errorState.username.error}
                helperText={
                  errorState.username.error ? errorState.username.message : ""
                }
                InputProps={
                  formState.username !== "" || selectState.username
                    ? {
                        startAdornment: (
                          <InputAdornment position="start">
                            <AccountCircleTwoToneIcon />
                          </InputAdornment>
                        ),
                      }
                    : {}
                }
              ></TextField>
              <TextField
                fullWidth
                label="Password Battle"
                value={formState.password}
                onChange={(e) => {
                  setformState({ ...formState, password: e.target.value });
                  seterrorState({
                    ...errorState,
                    password: { ...errorState.password, error: false },
                  });
                }}
                style={{ marginBottom: "2rem" }}
                variant="outlined"
                onFocus={() =>
                  setselectState({ ...selectState, password: true })
                }
                onBlur={() =>
                  setselectState({ ...selectState, password: false })
                }
                error={errorState.password.error}
                helperText={
                  errorState.password.error ? errorState.password.message : ""
                }
                InputProps={
                  formState.password !== "" || selectState.password
                    ? {
                        startAdornment: (
                          <InputAdornment position="start">
                            <VpnKeyTwoToneIcon />
                          </InputAdornment>
                        ),
                      }
                    : {}
                }
              ></TextField>
              <Paper
                style={{
                  margin: " 0 auto",
                  justifyContent: "center",
                  display: "flex",
                  background: "rgba(0,0,0,0)",
                  flexDirection: "column",
                }}
                elevation={0}
              >
                <Button
                  onSubmit={(e) => {
                    e.preventDefault();
                    handleLogin();
                  }}
                  style={{
                    textTransform: "none",
                    margin: "0 1.5rem",
                    borderRadius: "5px",
                    border: "2px solid #3298DC",
                    fontFamily: "Segoe UI",
                    fontWeight: "normal",
                    fontSize: "1.25rem",
                    color: "#3298DC",
                  }}
                  variant="outlined"
                  endIcon={
                    <Avatar
                      alt="g"
                      style={{ width: "1.5rem", height: "1.5rem" }}
                      variant="rounded"
                      src="https://image.flaticon.com/icons/png/512/222/222144.png"
                    />
                  }
                  type="submit"
                >
                  Masuk
                </Button>
              </Paper>
            </form>
          </CardContent>
        </Card>
      </Container>
    </div>
  );
}
