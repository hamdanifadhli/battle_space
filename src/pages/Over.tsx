import {
  Card,
  Container,
  CardContent,
  Avatar,
  Paper,
  Button,
} from "@material-ui/core";
import { makeStyles, createStyles, Theme } from "@material-ui/core/styles";
import React, { useState, useEffect } from "react";

const useStyle = makeStyles((theme: Theme) =>
  createStyles({
    avatar: {
      display: "flex",
      "& > *": {
        margin: theme.spacing(1),
      },
    },
  })
);

export default function Over() {
  const classes = useStyle();

  const [test, setTest] = useState({
    name: "",
    started_at: "",
    ends_at: "",
    image: "",
    subtest: [],
  });
  useEffect(() => {
    const testData = localStorage.getItem("test");
    if (typeof testData === "string") {
      const testData2 = JSON.parse(testData);
      setTest(testData2);
    }
    localStorage.removeItem("route");
  }, []);
  return (
    <Container maxWidth="md" style={{ marginTop: "5vh" }}>
      <Card
        style={{
          height: "90vh",
          textAlign: "center",
          margin: " 0 auto",
          justifyContent: "center",
          alignItems: "center",
          display: "flex",
          background: "rgba(0,0,0,0)",
          flexDirection: "column",
        }}
        elevation={0}
      >
        <CardContent>
          <img
            src="https://image.freepik.com/free-vector/finish-line-concept-illustration_114360-1820.jpg"
            style={{ height: "40vh" }}
            alt=""
          />
          <h1 style={{ textTransform: "capitalize" }}>
            Selamat, {localStorage.getItem("name")}
          </h1>
          <p style={{ fontSize: "1.4rem", margin: "0 0 1rem 0" }}>
            Kamu baru saja menyelesaikan {test.name}, pantau terus website dan
            media sosial kami untuk info lebih lanjut!
          </p>
          <Button
            style={{ textTransform: "none" }}
            color="primary"
            variant="outlined"
            onClick={() => {
              window.location.replace("https://studybuddy.id/");
            }}
          >
            Klik disini untuk kembali
          </Button>
          <p style={{ fontSize: "1.2rem", margin: "1rem 0 0.2rem 0" }}>
            Our Social Media
          </p>
          <Paper
            style={{
              margin: " 0 auto",
              justifyContent: "center",
              alignItems: "center",
              display: "flex",
              background: "rgba(0,0,0,0)",
              flexDirection: "column",
            }}
            elevation={0}
          >
            <div className={classes.avatar}>
              <a href="https://twitter.com/idstudybuddy">
                <Avatar
                  alt="Twitter"
                  src="https://www.glazingsummit.co.uk/wp-content/uploads/2019/09/app-icons-twitter.png"
                />
              </a>
              <a href="https://www.instagram.com/id.studybuddy/">
                <Avatar
                  alt="Instagram"
                  src="https://play-lh.googleusercontent.com/h9jWMwqb-h9hjP4THqrJ50eIwPekjv7QPmTpA85gFQ10PjV02CoGAcYLLptqd19Sa1iJ"
                />
              </a>
              <a href="https://line.me/ti/p/@krv7008k">
                <Avatar
                  alt="Line"
                  src="https://play-lh.googleusercontent.com/BkvRJsjYiEjb0-XKuop2AurqFKLhhu_iIP06TrCTGAq180P9Briv8Avz8ncLp7bOmCs"
                />
              </a>
            </div>
          </Paper>
        </CardContent>
      </Card>
    </Container>
  );
}
