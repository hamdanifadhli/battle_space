import { Grid, Hidden } from "@material-ui/core";
import TrialInfo from "../components/TrialInfo";
import TrialQuestion from "../components/TrialQuestion";
import { useState } from "react";

interface TrialProps {
  refresh: () => void;
}

export default function Trial(props: TrialProps) {
  const { refresh } = props;
  function re() {
    refresh();
  }
  const [soal, setSoal] = useState([
    {
      no: 1,
      soal: "soal 1",
      dijawab: false,
      jawaban: "",
    },
    {
      no: 2,
      dijawab: false,
      jawaban: "",
      soal: "soal 2",
    },
    { no: 3, soal: "soal 3", dijawab: false, jawaban: "" },
    {
      no: 4,
      dijawab: false,
      jawaban: "",
      soal: "soal 4",
    },
    { no: 5, soal: "soal 5", dijawab: false, jawaban: "" },
  ]);
  const [index, setIndex] = useState(0);

  console.log({ soal });
  return (
    <Grid container>
      {/* ini informasi panel versi desktop */}
      <Hidden smDown>
        <Grid container item md={3} style={{ padding: "1rem" }}>
          <div>
            <TrialInfo
              refresh={re}
              kumpulanSoal={soal}
              setSoal={setSoal}
              index={index}
              setIndex={setIndex}
            />
          </div>
        </Grid>
      </Hidden>
      <Grid container item sm={12} md={9} style={{ padding: "1rem" }}>
        <TrialQuestion
          refresh={re}
          kumpulanSoal={soal}
          setSoal={setSoal}
          index={index}
          setIndex={setIndex}
        />
      </Grid>
      {/* ini informasi panel versi mobile */}
      <Hidden mdUp>
        <Grid container item sm={12} style={{ padding: "1rem" }}>
          <TrialInfo
            refresh={re}
            kumpulanSoal={soal}
            setSoal={setSoal}
            index={index}
            setIndex={setIndex}
          />
        </Grid>
      </Hidden>
    </Grid>
  );
}
