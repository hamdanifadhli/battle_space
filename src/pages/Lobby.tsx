import {
  Grid,
  Card,
  CardContent,
  CardActions,
  CardHeader,
  Button,
  Paper,
  Typography,
  Table,
  TableBody,
  TableRow,
  TableCell,
  IconButton,
} from "@material-ui/core";
import { makeStyles, Theme, createStyles } from "@material-ui/core/styles";
import Loader from "react-loader-spinner";
import { useState, useEffect } from "react";
import CloseIcon from "@material-ui/icons/Close";
import { API } from "../configs/API";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    image: {
      width: "100%",
      height: "auto",
      [theme.breakpoints.down("sm")]: {
        width: "80%",
      },
      [theme.breakpoints.down("xs")]: {
        width: "100%",
      },
    },
  })
);

interface LobbyProps {
  refresh: () => void;
}

export default function Lobby(props: LobbyProps) {
  const classes = useStyles();
  const { refresh } = props;
  const [guide, setGuide] = useState(false);
  const [siswa, setsiswa] = useState({ name: "" });
  const [test, setTest] = useState({
    name: "",
    started_at: "",
    ends_at: "",
    image: "",
    subtest: [],
  });

  useEffect(() => {
    const testData = localStorage.getItem("test");
    if (typeof testData === "string") {
      const testData2 = JSON.parse(testData);
      setTest(testData2);
    }
    const name = localStorage.getItem("name");
    if (typeof name === "string") {
      const nama = name;
      setsiswa({ name: nama });
    }
  }, []);

  return (
    <div style={{ margin: "2rem 2rem" }}>
      <Grid container spacing={4}>
        <Grid item xs={12} md={3}>
          <Card>
            <CardContent>
              <Paper
                style={{
                  margin: " 0 auto",
                  justifyContent: "center",
                  alignItems: "center",
                  display: "flex",
                  background: "rgba(0,0,0,0)",
                  flexDirection: "column",
                }}
                elevation={0}
              >
                <img src={`${test.image}`} className={classes.image} alt="" />
              </Paper>
              <Typography
                variant="h5"
                style={{ margin: "0.5rem 0 0 0", fontWeight: 500 }}
                align="center"
              >
                {test.name}
              </Typography>
              <Table>
                <TableBody>
                  <TableRow>
                    <TableCell>
                      <b>Nama</b>
                    </TableCell>
                    <TableCell>:</TableCell>
                    <TableCell style={{ textTransform: "capitalize" }}>
                      {siswa.name}
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell>
                      <b>Dimulai</b>
                    </TableCell>
                    <TableCell>:</TableCell>
                    <TableCell>
                      {new Date(test.started_at).toLocaleString("id-ID", {
                        day: "numeric",
                        month: "long",
                        year: "numeric",
                        hour: "numeric",
                        minute: "numeric",
                        weekday: "long",
                        timeZone: "Asia/Jakarta",
                      })}
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell>
                      <b>Ditutup</b>
                    </TableCell>
                    <TableCell>:</TableCell>
                    <TableCell>
                      {new Date(test.ends_at).toLocaleString("id-ID", {
                        day: "numeric",
                        month: "long",
                        year: "numeric",
                        hour: "numeric",
                        minute: "numeric",
                        timeZone: "Asia/Jakarta",
                        weekday: "long",
                      })}
                    </TableCell>
                  </TableRow>
                </TableBody>
              </Table>
            </CardContent>
            <CardActions style={{ padding: 0 }}>
              <Button
                variant="contained"
                style={{
                  background: "#eef3fc",
                  boxSizing: "border-box",
                  boxShadow: "2px 2px 3px rgba(0, 0, 0, 0.25)",
                  borderRadius: "4px",
                  color: "#2160c4",
                }}
                fullWidth
                onClick={() => {
                  localStorage.removeItem("token");
                  localStorage.setItem("route", "1");
                  window.location.href = "/";
                }}
              >
                Logout
              </Button>
            </CardActions>
          </Card>
        </Grid>
        <Grid item xs={12} md={9}>
          <Grid container spacing={4}>
            <Grid item xs={12}>
              <Card
                style={{
                  background: "linear-gradient(to right, #5b86e5, #36d1dc)",
                  color: "#FFF",
                }}
              >
                <CardContent style={{ padding: "52px" }}>
                  <Paper
                    style={{
                      margin: " 0 auto",
                      justifyContent: "center",
                      display: "flex",
                      background: "rgba(0,0,0,0)",
                      color: "#FFF",
                      flexDirection: "column",
                    }}
                    elevation={0}
                  >
                    <Typography
                      variant="h5"
                      style={{
                        fontSize: "2rem",
                        textTransform: "capitalize",
                      }}
                      align="left"
                    >
                      Halo, {siswa.name}
                    </Typography>
                    <Typography
                      variant="h5"
                      style={{ fontSize: "25px" }}
                      align="left"
                    >
                      Selamat berjuang di {test.name}!
                    </Typography>
                  </Paper>
                </CardContent>
              </Card>
            </Grid>
            <Grid
              item
              xs={12}
              md={4}
              style={{ display: guide ? "none" : "flex" }}
            >
              <div>
                <Card>
                  <CardContent>
                    <Paper
                      style={{
                        margin: " 0 auto",
                        justifyContent: "center",
                        alignItems: "center",
                        display: "flex",
                        background: "rgba(0,0,0,0)",
                        color: "#FFF",
                        flexDirection: "column",
                      }}
                      elevation={0}
                    >
                      <Loader
                        type="Circles"
                        color="#00BFFF"
                        height={140}
                        width={140}
                      />
                    </Paper>
                    <Typography
                      variant="h5"
                      style={{ fontSize: "32px", lineHeight: "53px" }}
                      align="left"
                    >
                      Quick Guide
                    </Typography>
                    <Typography
                      variant="body1"
                      align="left"
                      style={{ fontSize: "15px", lineHeight: "25px" }}
                    >
                      Jawaban aku kesimpen nggak ya? Hasil battle ini langsung
                      muncul gak yah? Yuk baca dulu sedikit biar tau jawabannya!
                    </Typography>
                  </CardContent>
                  <CardActions style={{ padding: 0 }} disableSpacing>
                    <Button
                      variant="contained"
                      style={{
                        background: "#E9F7EF",
                        boxSizing: "border-box",
                        boxShadow: "2px 2px 3px rgba(0, 0, 0, 0.25)",
                        borderRadius: "4px",
                        color: "#008038",
                      }}
                      fullWidth
                      onClick={() => setGuide(!guide)}
                    >
                      Baca Guide
                    </Button>
                  </CardActions>
                </Card>
              </div>
            </Grid>
            <Grid
              item
              xs={12}
              md={4}
              style={{ display: guide ? "none" : "flex" }}
            >
              <div>
                <Card>
                  <CardContent>
                    <Paper
                      style={{
                        margin: " 0 auto",
                        justifyContent: "center",
                        alignItems: "center",
                        display: "flex",
                        background: "rgba(0,0,0,0)",
                        color: "#FFF",
                        flexDirection: "column",
                      }}
                      elevation={0}
                    >
                      <Loader
                        type="Rings"
                        color="#00BFFF"
                        height={140}
                        width={140}
                      />
                    </Paper>
                    <Typography
                      variant="h5"
                      style={{ fontSize: "32px", lineHeight: "53px" }}
                      align="left"
                    >
                      Pseudo Battle
                    </Typography>
                    <Typography
                      variant="body1"
                      align="left"
                      style={{ fontSize: "15px", lineHeight: "25px" }}
                    >
                      Take care of your tools and they will take care of you.
                      Sebelum mulai, yuk biasakan diri kamu sama tampilan battle
                      kami!
                    </Typography>
                  </CardContent>
                  <CardActions style={{ padding: 0 }} disableSpacing>
                    <Button
                      variant="contained"
                      style={{
                        background: "#E9F7EF",
                        boxSizing: "border-box",
                        boxShadow: "2px 2px 3px rgba(0, 0, 0, 0.25)",
                        borderRadius: "4px",
                        color: "#008038",
                      }}
                      fullWidth
                      onClick={() => {
                        localStorage.setItem("route", "2");
                        refresh();
                      }}
                    >
                      Coba Battle
                    </Button>
                  </CardActions>
                </Card>
              </div>
            </Grid>
            <Grid
              item
              xs={12}
              md={4}
              style={{ display: guide ? "none" : "flex" }}
            >
              <div>
                <Card>
                  <CardContent>
                    <Paper
                      style={{
                        margin: " 0 auto",
                        justifyContent: "center",
                        alignItems: "center",
                        display: "flex",
                        background: "rgba(0,0,0,0)",
                        color: "#FFF",
                        flexDirection: "column",
                      }}
                      elevation={0}
                    >
                      <Loader
                        type="Grid"
                        color="#00BFFF"
                        height={140}
                        width={140}
                      />
                    </Paper>
                    <Typography
                      variant="h5"
                      style={{ fontSize: "32px", lineHeight: "53px" }}
                      align="left"
                    >
                      Start?
                    </Typography>
                    <Typography
                      variant="body1"
                      align="left"
                      style={{ fontSize: "15px", lineHeight: "25px" }}
                    >
                      Sudah siap? sudah waktunya? Siapkan koneksi internet, alat
                      tulis, dan semangat terbaikmu! Best wishes champ, selamat
                      dan sukses!
                    </Typography>
                  </CardContent>
                  <CardActions style={{ padding: 0 }} disableSpacing>
                    {new Date().getTime() >
                      new Date(test.started_at).getTime() &&
                    new Date().getTime() < new Date(test.ends_at).getTime() ? (
                      <Button
                        variant="contained"
                        style={{
                          background: "#E9F7EF",
                          boxSizing: "border-box",
                          boxShadow: "2px 2px 3px rgba(0, 0, 0, 0.25)",
                          borderRadius: "4px",
                          color: "#008038",
                        }}
                        fullWidth
                        onClick={() => {
                          localStorage.setItem("route", "3");
                          refresh();
                        }}
                      >
                        Mulai
                      </Button>
                    ) : (
                      <Button
                        variant="contained"
                        style={{
                          background: "#fffbeb",
                          boxSizing: "border-box",
                          boxShadow: "2px 2px 3px rgba(0, 0, 0, 0.25)",
                          borderRadius: "4px",
                          color: "#947600",
                        }}
                        fullWidth
                        disabled
                      >
                        {new Date().getTime() <
                        new Date(test.started_at).getTime()
                          ? "Belum Dimulai"
                          : "Sudah berakhir"}
                      </Button>
                    )}
                  </CardActions>
                </Card>
              </div>
            </Grid>
            <Grid item xs={12} style={{ display: guide ? "flex" : "none" }}>
              <Card style={{ width: "100%" }}>
                <CardHeader
                  title="Quick Guide"
                  action={
                    <IconButton
                      aria-label="settings"
                      onClick={() => setGuide(!guide)}
                    >
                      <CloseIcon />
                    </IconButton>
                  }
                ></CardHeader>
                <CardContent>
                  <Grid container spacing={6}>
                    <Grid item xs={12} md={6}>
                      <Typography
                        variant="h5"
                        style={{ fontSize: "25px", lineHeight: "43px" }}
                        align="left"
                      >
                        Timing
                      </Typography>
                      <Typography
                        variant="body1"
                        align="left"
                        style={{ fontSize: "15px", lineHeight: "25px" }}
                      >
                        Battle ini kami buat semirip mungkin dengan UTBK yang
                        sebenarnya biar kamu bisa lebih familiar sama UI (
                        <em>User Interface</em> ) salah satu ujian terpenting
                        dalam hidup kamu! Setiap battle akan terdiri dari satu
                        atau lebih subtes dengan jeda otomatis 30 detik diantara
                        setiap subtes. Nah, jadi kamu manfaatin waktu itu
                        baik-baik ya.
                      </Typography>
                      <Typography
                        variant="h5"
                        style={{ fontSize: "25px", lineHeight: "43px" }}
                        align="left"
                      >
                        Aplikasi
                      </Typography>
                      <Typography
                        variant="body1"
                        align="left"
                        style={{ fontSize: "15px", lineHeight: "25px" }}
                      >
                        Battle ini dilengkapi dengan
                        <em>Client Side Rendering</em> untuk meminimalisir jeda
                        loading antara satu soal ke soal yang lain. Hal ini
                        dilakukan untuk menyimulasikan pengalaman menggunakan
                        aplikasi UTBK yang sebenarnya. Tidak akan ada jeda dari
                        lomapatan satu soal ke yang lain, jangan kaget ya!
                      </Typography>
                      <Typography
                        variant="h5"
                        style={{ fontSize: "25px", lineHeight: "43px" }}
                        align="left"
                      >
                        Catatan
                      </Typography>
                      <Typography
                        variant="body1"
                        align="left"
                        style={{ fontSize: "15px", lineHeight: "25px" }}
                      >
                        Setiap kamu menjawab satu soal, kami langsung menyimpan
                        jawabanmu dalam database kami. Kami juga menyimpan waktu
                        yang kamu habiskan dalam mengerjakan satu soal. Jadi,
                        tinggal pastiin aja kamu nyambung ke internet dan
                        sekarang udah ada lagi kekhawatiran 'Aduh, udah kesubmit
                        belom yah?'
                      </Typography>
                    </Grid>
                    <Grid item xs={12} md={6}>
                      <Typography
                        variant="h5"
                        style={{ fontSize: "25px", lineHeight: "43px" }}
                        align="left"
                      >
                        Good Luck!
                      </Typography>
                      <Typography
                        variant="body1"
                        align="left"
                        style={{ fontSize: "15px", lineHeight: "25px" }}
                      >
                        Hasil dari Battle ini tidak akan langsung muncul setelah
                        kamu selesai mengerjakannya. Kami membutuhkan waktu
                        untuk menganalisa performa kalian serta menghitung skor
                        kalian menggunakan IRT.
                      </Typography>
                      <Typography
                        variant="body1"
                        align="left"
                        style={{ fontSize: "15px", lineHeight: "25px" }}
                      >
                        Oh, dan ingat juga bud, ini bukan UTBK. Kami memberikan
                        sebuah 'simulasi' UTBK untuk memberimu sarana untuk
                        melatih dan membiasakan dirimu dengan yang sesungguhnya.
                        Saran kami, jangan jadikan hasil dari battle ini sebagai
                        gambaran skor kamu saat UTBK nanti karena penilaian
                        menggunakan IRT akan menghasilkan
                        <em>scoring</em> yang berbeda-beda pada setiap tes.
                        Dengan itu, semangat Buddy!
                      </Typography>
                      <Typography
                        variant="body1"
                        align="left"
                        style={{ fontSize: "15px", lineHeight: "25px" }}
                      >
                        Salam hangat, Tim Studybuddy
                      </Typography>
                    </Grid>
                  </Grid>
                </CardContent>
              </Card>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </div>
  );
}
