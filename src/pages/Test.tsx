import {
  Grid,
  Hidden,
  Button,
  Fade,
  Dialog,
  DialogContent,
  DialogContentText,
  DialogActions,
} from "@material-ui/core";
import Examquestion from "../components/Examquestion";
import InformationCard from "../components/Information";
import { TestInfo, SoalInterface } from "../interface/test";
import { useState, useEffect } from "react";
import axios from "axios";
import { API } from "../configs/API";
import { Loading } from "../components/exam_component/Loading";
import { Shuffle } from "../functions/exam_func/Loading_help";
import axiosRetry from "axios-retry";

interface TestProps {
  refresh: () => void;
}

export default function Test(props: TestProps) {
  axiosRetry(axios, {
    retries: 3,
    retryDelay: (retryCount) => {
      return 3000;
    },
  });
  const { refresh } = props;
  function re() {
    refresh();
  }
  const [alertRefresh, setAlertRefresh] = useState(false);
  const handleRefresh = () => {
    setAlertRefresh(false);
  };
  const handleRefreshOpen = () => {
    setAlertRefresh(true);
  };
  const [testState, settestState] = useState<TestInfo>({
    id: 0,
    nama: "",
    index: 0,
    subtes: "",
    duration: 999999,
    image: "",
    timeStamp: 0,
    listSubtes: [""],
  });
  const [soal, setSoal] = useState<Array<SoalInterface>>([
    {
      order: 1,
      question: "",
      dijawab: false,
      jawaban: "",
      answer: 99,
      options: [{ text: "", order: 0 }],
    },
  ]);
  const [timerOn, setTimerOn] = useState<boolean>(false);
  const [loading, setLoading] = useState<boolean>(false);

  useEffect(() => {
    if (soal.length > 1) {
      try {
        localStorage.setItem("kecapxyz", JSON.stringify(soal));
      } catch (error) {
        console.log(error);
      }
    }
  }, [soal]);

  useEffect(() => {
    const testData = localStorage.getItem("test");
    const testID = localStorage.getItem("t_id");
    const lalala = localStorage.getItem("lalala");
    if (typeof lalala === "string") {
      const lalala2: TestInfo = JSON.parse(lalala);
      lalala2.index = 0;
      settestState(lalala2);
    } else {
      if (typeof testData === "string" && typeof testID === "string") {
        const testData2 = JSON.parse(testData);
        const testID2 = testID;
        settestState({
          ...testState,
          index: 0,
          listSubtes: testData2.subtest,
          id: testID2,
          nama: testData2.name,
          image: testData2.image,
          subtes: testData2.subtest[0],
        });
      }
    }
  }, []);

  useEffect(() => {
    if (testState.id !== 0) {
      localStorage.setItem("lalala", JSON.stringify(testState));
    }
  }, [testState]);

  function miliKeMenitDetik(mili: number) {
    var menit = Math.floor(mili / 60000);
    var detik = Math.floor((mili % 60000) / 1000);
    if (timerOn === false) {
      setTimerOn(true);
      StartTimer(menit, detik, testState.listSubtes[0]);
    }
  }

  let intervalId: NodeJS.Timeout;

  function NextSubtest() {
    if (testState.listSubtes.length > 1) {
      setLoading(true);
      setTimeout(() => {
        let a = testState.listSubtes;
        a.shift();
        settestState({
          ...testState,
          listSubtes: a !== undefined ? a : testState.listSubtes,
          index: 0,
        });
        let b = a[0];
        if (testState.id === 0) {
        } else {
          axios
            .get(
              `https://wahyuboard.my.id/study/content/test/get/subtest/${b}/${testState.id}`
            )
            .then(function (response) {
              setSoal(Shuffle(response.data.question));
              settestState({
                ...testState,
                duration: response.data.duration,
                timeStamp: new Date().getTime(),
                subtes: b,
                index: 0,
              });
            })
            .catch(function (error) {
              console.log(error);
              handleRefreshOpen();
            });
        }
        setLoading(false);
      }, 30000);
    } else {
      localStorage.setItem("route", "4");
      localStorage.removeItem("lalala");
      // localStorage.removeItem("u_id");
      // localStorage.removeItem("t_id");
      localStorage.removeItem("battleToken");
      localStorage.removeItem("kecapxyz");
      refresh();
    }
  }

  const [waktu, setwaktu] = useState({
    pu: { min: 0, sec: 0 },
    ppu: { min: 0, sec: 0 },
    pk: { min: 0, sec: 0 },
    pmm: { min: 0, sec: 0 },
    eng: { min: 0, sec: 0 },
    ma: { min: 0, sec: 0 },
    fi: { min: 0, sec: 0 },
    ki: { min: 0, sec: 0 },
    bi: { min: 0, sec: 0 },
    sos: { min: 0, sec: 0 },
    sej: { min: 0, sec: 0 },
    geo: { min: 0, sec: 0 },
    eko: { min: 0, sec: 0 },
  });
  function StartTimer(menit: number, detik: number, subtes: string) {
    let asu = { menit: menit, detik: detik };
    clearInterval(intervalId);
    intervalId = setInterval(() => {
      if (asu.menit <= 0 && asu.detik <= 0) {
        clearInterval(intervalId);
        setTimerOn(false);
        if (subtes === testState.subtes) {
          NextSubtest();
        }
      } else if (asu.detik === 0) {
        asu.menit = asu.menit - 1;
        asu.detik = 59;
      } else {
        asu.detik = asu.detik - 1;
      }
      switch (subtes) {
        case "pu":
          setwaktu({ ...waktu, pu: { min: asu.menit, sec: asu.detik } });
          break;
        case "ppu":
          setwaktu({ ...waktu, ppu: { min: asu.menit, sec: asu.detik } });
          break;
        case "pk":
          setwaktu({ ...waktu, pk: { min: asu.menit, sec: asu.detik } });
          break;
        case "pmm":
          setwaktu({ ...waktu, pmm: { min: asu.menit, sec: asu.detik } });
          break;
        case "eng":
          setwaktu({ ...waktu, eng: { min: asu.menit, sec: asu.detik } });
          break;
        case "ma":
          setwaktu({ ...waktu, ma: { min: asu.menit, sec: asu.detik } });
          break;
        case "fi":
          setwaktu({ ...waktu, fi: { min: asu.menit, sec: asu.detik } });
          break;
        case "ki":
          setwaktu({ ...waktu, ki: { min: asu.menit, sec: asu.detik } });
          break;
        case "bi":
          setwaktu({ ...waktu, bi: { min: asu.menit, sec: asu.detik } });
          break;
        case "sos":
          setwaktu({ ...waktu, sos: { min: asu.menit, sec: asu.detik } });
          break;
        case "sej":
          setwaktu({ ...waktu, sej: { min: asu.menit, sec: asu.detik } });
          break;
        case "eko":
          setwaktu({ ...waktu, eko: { min: asu.menit, sec: asu.detik } });
          break;
        case "geo":
          setwaktu({ ...waktu, geo: { min: asu.menit, sec: asu.detik } });
          break;
      }
    }, 1000);
  }
  useEffect(() => {
    const kecap = localStorage.getItem("kecapxyz");
    if (typeof kecap === "string") {
      const kecap2 = JSON.parse(kecap);
      if (testState.id === 0) {
      } else {
        // https://wahyuboard.my.id/study/content/test/get/subtest/${testState.listSubtes[0]}/${testState.id}
        axios
          .get(
            `https://wahyuboard.my.id/study/content/test/get/subtest/${testState.listSubtes[0]}/${testState.id}`
          )
          .then(function (response) {
            let sama = false;
            let soalbaru = response.data.question[0];
            for (let i = 0; i < kecap2.length; i++) {
              if (soalbaru.soal === kecap2[i].soal) {
                sama = true;
              }
            }
            if (sama === true) {
              setSoal(kecap2);
            } else {
              setSoal(Shuffle(response.data.question));
              settestState({
                ...testState,
                duration: response.data.duration,
                timeStamp: new Date().getTime(),
                index: 0,
              });
            }
          })
          .catch(function (error) {
            console.log(error);
            handleRefreshOpen();
          });
      }
    } else {
      if (testState.id === 0) {
      } else {
        axios
          .get(
            `https://wahyuboard.my.id/study/content/test/get/subtest/${testState.listSubtes[0]}/${testState.id}`
          )
          .then(function (response) {
            setSoal(Shuffle(response.data.question));
            settestState({
              ...testState,
              duration: response.data.duration,
              timeStamp: new Date().getTime(),
              index: 0,
            });
          })
          .catch(function (error) {
            console.log(error);
            handleRefreshOpen();
          });
      }
    }
  }, [testState.id, testState.subtes]);

  useEffect(() => {
    if (testState.duration === 999999) {
    } else {
      let akhir = testState.timeStamp + testState.duration * 1000;
      let awal = new Date().getTime();
      miliKeMenitDetik(akhir - awal);
    }
  }, [testState.duration, testState.timeStamp]);

  return (
    <div>
      <Fade in={!loading} style={{ display: loading ? "none" : "" }}>
        <Grid container>
          {/* ini informasi panel versi desktop */}
          <Hidden smDown>
            <Grid container item md={3} style={{ padding: "1rem" }}>
              <div>
                <InformationCard
                  refresh={re}
                  kumpulanSoal={soal}
                  setSoal={setSoal}
                  testState={testState}
                  setTestState={settestState}
                  waktu={waktu}
                  setTimer={setTimerOn}
                  nextSubtes={NextSubtest}
                  handleRefreshOpen={handleRefreshOpen}
                />
              </div>
            </Grid>
          </Hidden>
          <Grid container item sm={12} md={9} style={{ padding: "1rem" }}>
            <Examquestion
              refresh={re}
              kumpulanSoal={soal}
              setSoal={setSoal}
              waktu={waktu}
              testState={testState}
              setTestState={settestState}
              setTimer={setTimerOn}
              nextSubtes={NextSubtest}
              handleRefreshOpen={handleRefreshOpen}
            />
          </Grid>
          {/* ini informasi panel versi mobile */}
          <Hidden mdUp>
            <Grid container item sm={12} style={{ padding: "1rem" }}>
              <InformationCard
                refresh={re}
                kumpulanSoal={soal}
                setSoal={setSoal}
                waktu={waktu}
                testState={testState}
                setTestState={settestState}
                setTimer={setTimerOn}
                nextSubtes={NextSubtest}
                handleRefreshOpen={handleRefreshOpen}
              />
            </Grid>
          </Hidden>
        </Grid>
      </Fade>
      <Fade in={loading}>
        <div style={{ display: loading ? "" : "none" }}>
          <Loading />
        </div>
      </Fade>
      <Dialog
        open={alertRefresh}
        onClose={handleRefresh}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            Tolong refresh browser anda.
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button
            onClick={() => {
              handleRefresh();
              window.location.reload();
            }}
            color="primary"
            autoFocus
          >
            Setuju
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
